using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Script that moves the camera on Y axis if the player is too far up
    private const float DISTANCE_MULTIPLIER = 0.002f;
    private const float MINIMUM_VELOCITY = 0.001f;

    [SerializeField] private PlayerController player;
    [SerializeField] private float followDistance = 3;

    private float velocity = 0;
    private float screenWidth;
    private float screenHeight;

    public float GetScreenWidth()
    {
        return screenWidth;
    }

    public float GetScreenHeight()
    {
        return screenHeight;
    }

    private void AddVelocity()
    {
        transform.position += new Vector3(0, velocity);
    }

    private void Awake() 
    {
        // Get camera screen width and height, assuming that camera is at (0, 0)
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        screenWidth = position.x * 2;
        screenHeight = position.y * 2;
    }

    private void Start() 
    {
        // Move camera to starting position
        transform.position += new Vector3(0, 4, 0);
    }

    private void Update()
    {
        float distanceToPlayer = player.GetHeight() - transform.position.y;

        if (distanceToPlayer > followDistance)
        {
            velocity = distanceToPlayer * DISTANCE_MULTIPLIER;
            /*if (player.GetPosition().y > transform.position.y)
            {
                velocity = distanceToPlayer * DISTANCE_MULTIPLIER;
            }
            else
            {
                velocity = -distanceToPlayer * DISTANCE_MULTIPLIER;
            }*/

            if (Mathf.Abs(velocity) < MINIMUM_VELOCITY)
                velocity = MINIMUM_VELOCITY * Mathf.Sign(velocity);
        }
        else
        {
            if (Mathf.Abs(velocity) > MINIMUM_VELOCITY)
                velocity -= MINIMUM_VELOCITY * Mathf.Sign(velocity);
            else
                velocity = 0;
        }

        AddVelocity();
    }
}
