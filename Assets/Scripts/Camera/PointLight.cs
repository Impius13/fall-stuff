using UnityEngine;

public class PointLight : MonoBehaviour
{
    // Controls position of the light source
    private float screenHeight;

    private void Start() 
    {
        CameraFollow camera = Camera.main.GetComponent<CameraFollow>();
        screenHeight = camera.GetScreenHeight();
    }

    private void Update() 
    {
        // Move light on top of camera
        Vector3 newPosition = Camera.main.transform.position;
        newPosition.z = 0;
        newPosition.y += screenHeight;
        transform.position = newPosition;
    }
}
