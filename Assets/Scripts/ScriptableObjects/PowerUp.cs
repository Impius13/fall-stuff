using UnityEngine;

[CreateAssetMenu(fileName = "PowerUp", menuName = "ScriptableObjects/PowerUp")]
public class PowerUp : ScriptableObject
{
    public enum PowerUpType 
    {
        Life,
        DoubleJump,
        SlowTime,
        Speed
    }

    public float time;
    public float value;
    public PowerUpType type;
    public Sprite icon;
}
