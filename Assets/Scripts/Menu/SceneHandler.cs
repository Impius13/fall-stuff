using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    [SerializeField] private RectTransform fader;

    public delegate void OnFinishedSceneTransition();
    public OnFinishedSceneTransition onFinishedSceneTransition;

    public void StartGame()
    {
        fader.gameObject.SetActive(true);

        // Use darken the screen then load game scene
        LeanTween.alpha(fader, 0, 0);
        LeanTween.alpha(fader, 1, 0.5f).setOnComplete(() => {
            Invoke("LoadGame", 0.5f);
        });
    }

    public void LoadMenu()
    {
        fader.gameObject.SetActive(true);
        
        // Use darken the screen then load menu scene
        LeanTween.alpha(fader, 0, 0);
        LeanTween.alpha(fader, 1, 0.5f).setOnComplete(() => {
            SceneManager.LoadScene(0);
        });
    }

    public void EndGame()
    {
        // Ends the game
        Application.Quit();
    }

    private void LoadGame() 
    {
        SceneManager.LoadScene(1);
    }

    private void Start() 
    {
        fader.gameObject.SetActive(true);

        // Use LeanTween to change alpha to zero
        LeanTween.alpha(fader, 1, 0);
        LeanTween.alpha(fader, 0, 0.5f).setOnComplete(() => {
            fader.gameObject.SetActive(false);
            // Invoke the delegate methods
            if (onFinishedSceneTransition != null)
                onFinishedSceneTransition.Invoke();
        });
    }
}
