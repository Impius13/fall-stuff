using System.Collections.Generic;
using UnityEngine;

// Generate menu background scene
public class MenuBackground : MonoBehaviour
{
    private const float DISTANCE_TO_SPAWN_SEGMENT = 30f;
    private const int MAX_SEGMENT_COUNT = 3;

    [SerializeField] private float timeToSpawnStuff;

    private SegmentSpawner segmentSpawner;
    private ObjectSpawner spawner;
    private StuffPreset stuffPreset;

    private List<Transform> currentBackgroundSegments;
    private float currentTimeToSpawn;

    private void Awake() 
    {
        segmentSpawner = GetComponent<SegmentSpawner>();
        spawner = GetComponent<ObjectSpawner>();
        stuffPreset = GetComponent<StuffPreset>();
        
        currentBackgroundSegments = new List<Transform>();

        // Get screen width and height
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        // Set spawner area
        spawner.SetAreaWidth(position.x * 2);
        spawner.SetAreaHeight(position.y * 2);

        currentTimeToSpawn = Time.time + timeToSpawnStuff;
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, segmentSpawner.GetLastSegmentEndPosition()) < 30f)
        {
            // Spawn next background segment
            Transform newSegment = segmentSpawner.SpawnSegment();
            currentBackgroundSegments.Add(newSegment);
        }

        if (currentBackgroundSegments.Count > MAX_SEGMENT_COUNT) {
            // Remove last segment
            Transform lastSegment = currentBackgroundSegments[0];
            currentBackgroundSegments.Remove(lastSegment);
            Destroy(lastSegment.gameObject);
        }

        if (Time.time > currentTimeToSpawn) {
            // Spawn random stuff
            Transform spawnedObj = spawner.SpawnObjectBackgroundMenu(stuffPreset.GetRandomObject());
            spawner.RandomizeRotation(spawnedObj);
            currentTimeToSpawn = Time.time + timeToSpawnStuff + UnityEngine.Random.Range(-0.5f, 2f);
        }
    }
}
