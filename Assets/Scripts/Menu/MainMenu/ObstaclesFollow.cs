using UnityEngine;

public class ObstaclesFollow : MonoBehaviour
{
    const float Y_CAMERA_DISTANCE = 8.7f;
    
    private void Update() 
    {
        // Keep obstacles some distance above camera
        transform.position = Camera.main.transform.position;
        transform.position += new Vector3(0, Y_CAMERA_DISTANCE, 0);
    }
}
