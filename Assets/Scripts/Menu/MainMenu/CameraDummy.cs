using UnityEngine;

public class CameraDummy : MonoBehaviour
{
    [SerializeField] private Transform objectFollow;

    private void Update() {
        transform.position = new Vector3(objectFollow.position.x, objectFollow.position.y, -14);
    }
}
