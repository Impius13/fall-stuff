using UnityEngine;

public class DummyStuffLifespan : MonoBehaviour
{
    void Update()
    {
        // Destroy self if it's too far from camera
        if (Vector3.Distance(transform.position, Camera.main.transform.position) > 30) {
            Destroy(gameObject);
        }
    }
}
