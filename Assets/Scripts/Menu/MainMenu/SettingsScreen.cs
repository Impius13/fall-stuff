using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

// Controls for the settings screen
public class SettingsScreen : MonoBehaviour
{
    [SerializeField] private GameObject settingsScreen;
    [SerializeField] private GameObject mainMenuScreen;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Slider masterSlider, musicSlider, sfxSlider;
    [SerializeField] private TextMeshProUGUI masterValue, musicValue, sfxValue;

    public void ShowSettings()
    {
        mainMenuScreen.SetActive(false);
        settingsScreen.SetActive(true);
    }

    public void ShowMenu()
    {
        mainMenuScreen.SetActive(true);
        settingsScreen.SetActive(false);
    }

    public void SetMasterVol()
    {
        masterValue.text = ConvertRange(-60, 0, 0, 100, (int)masterSlider.value).ToString();

        audioMixer.SetFloat("MasterVol", masterSlider.value);
        // Also save the value in player prefs
        PlayerPrefs.SetFloat("MasterVol", masterSlider.value);
    }

    public void SetMusicVol()
    {
        musicValue.text = ConvertRange(-60, 0, 0, 100, (int)musicSlider.value).ToString();

        audioMixer.SetFloat("MusicVol", musicSlider.value);
        // Also save the value in player prefs
        PlayerPrefs.SetFloat("MusicVol", musicSlider.value);
    }

    public void SetSFXVol()
    {
        sfxValue.text = ConvertRange(-60, 0, 0, 100, (int)sfxSlider.value).ToString();

        audioMixer.SetFloat("SFXVol", sfxSlider.value);
        // Also save the value in player prefs
        PlayerPrefs.SetFloat("SFXVol", sfxSlider.value);
    }

    private void Start() 
    {
        // Load values from player prefs and update sliders
        float vol = 0;
        audioMixer.GetFloat("MasterVol", out vol);
        masterSlider.value = vol;

        audioMixer.GetFloat("MusicVol", out vol);
        musicSlider.value = vol;

        audioMixer.GetFloat("SFXVol", out vol);
        sfxSlider.value = vol;

        // Also update text labels
        masterValue.text = ConvertRange(-60, 0, 0, 100, (int)masterSlider.value).ToString();
        musicValue.text = ConvertRange(-60, 0, 0, 100, (int)musicSlider.value).ToString();
        sfxValue.text = ConvertRange(-60, 0, 0, 100, (int)sfxSlider.value).ToString();
    }

    // Converts given value that falls under some range to a different range
    private  int ConvertRange(int originalStart, int originalEnd, // original range
        int newStart, int newEnd, // desired range
        int value) // value to convert
    {
        double scale = (double)(newEnd - newStart) / (originalEnd - originalStart);
        return (int)(newStart + ((value - originalStart) * scale));
    }
}
