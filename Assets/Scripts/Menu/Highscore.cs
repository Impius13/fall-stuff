using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Highscore : MonoBehaviour
{
    // Load and display saved highscore
    private TextMeshProUGUI highscoreText;

    private void Awake()
    {
        highscoreText = GetComponent<TextMeshProUGUI>();
        int score = PlayerPrefs.GetInt("highscore", 0);
        highscoreText.text = "Highscore\n" + score.ToString();
    }
}
