using UnityEngine;

public class SpriteController : MonoBehaviour
{
    // Controls properties of sprite
    private const float OPACITY_STEP_CHANGE = 0.2f;
    private const float REPEAT_STEP = 0.1f;

    private SpriteRenderer renderController;

    public void StaticEffect()
    {
        InvokeRepeating("DecreaseOpacity", 0, REPEAT_STEP);
    }

    private void DecreaseOpacity()
    {
        // Decrease opacity of sprite until it's 1/2
        if (renderController.color.a <= 0.5f)
        {
            CancelInvoke();
            InvokeRepeating("IncreaseOpacity", 0, REPEAT_STEP);
        }
        else
        {
            Color color = renderController.color;
            color.a -= OPACITY_STEP_CHANGE;
            renderController.color = color;
        }
    }

    private void IncreaseOpacity()
    {
        // Increase opacity of sprite until it's 1
        if (renderController.color.a == 1f)
        {
            CancelInvoke();
        }
        else
        {
            Color color = renderController.color;
            color.a += OPACITY_STEP_CHANGE;
            renderController.color = color;
        }
    }

    private void Awake() 
    {
        renderController = GetComponent<SpriteRenderer>();
    }
}
