using UnityEngine;

public class StuffCollision : MonoBehaviour
{
    // If stuff already fell, do not stun player if collision
    private const float TIME_AFTER_STATIC = 5;

    private SpriteController spriteController;
    private AudioSource sound;

    private bool fell = false;
    private bool isStatic = false;
    private bool impactSoundPlayed = false;
    private float timeToMakeStatic = 0;

    public bool Fell()
    {
        return fell;
    }

    public bool IsStatic()
    {
        return isStatic;
    }

    public void PlayImpactSound()
    {
        // Play the impact sound if it was not already played
        if (!impactSoundPlayed)
        {
            impactSoundPlayed = true;
            sound.Play();
        }
    }

    private void MakeStatic()
    {
        // Make the object static
        spriteController.StaticEffect();
        
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Static;
        isStatic = true;
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        LayerMask otherLayer = other.gameObject.layer;
        if (!fell && ((otherLayer == LayerMask.NameToLayer("Stuff") && other.gameObject.GetComponent<StuffCollision>().Fell())
            || otherLayer == LayerMask.NameToLayer("Ground")))
        {
            fell = true;
        }
    }

    private void Awake() 
    {
        spriteController = GetComponent<SpriteController>();
        sound = GetComponent<AudioSource>();
    }

    private void Update() 
    {
        // Make the stuff static some time after falling
        if (!isStatic && fell && timeToMakeStatic == 0)
            timeToMakeStatic = Time.time + TIME_AFTER_STATIC;

        if (!isStatic && timeToMakeStatic != 0 && Time.time > timeToMakeStatic)
            MakeStatic();

        if (isStatic &&
            Vector2.Distance(transform.position, Camera.main.transform.position) > Camera.main.GetComponent<CameraFollow>().GetScreenHeight())
        {
            // Destroy stuff if its some distance under camera
            Destroy(gameObject);
            this.enabled = false;
        }

        if (Vector2.Distance(transform.position, Camera.main.transform.position) > 50f)
        {
            // Destroy self and pointer if fell over wall
            GetComponent<DirectionPointer>().DestroyPointer();
            Destroy(gameObject);
            this.enabled = false;
        }
    }
}
