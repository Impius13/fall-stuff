using UnityEngine;
using UnityEngine.UI;

public class DirectionPointer : MonoBehaviour
{
    // Script that spawns pointer arrow indicating from where stuff will fall
    private const float BORDER_SIZE = 53;

    [SerializeField] private RectTransform pointerPrefab;
    [SerializeField] private Sprite iconSprite;
    
    private Vector3 targetPosition;
    private RectTransform pointerWindow;
    private RectTransform pointer;

    public void DestroyPointer()
    {
        Destroy(pointerWindow.gameObject);
        this.enabled = false;
    }

    private void Awake() 
    {
        // Create PointerWindow and get the pointer arrow from it
        pointerWindow = Instantiate(pointerPrefab, pointerPrefab.position, Quaternion.identity);
        pointerWindow.SetParent(GameObject.Find("UI").transform);
        pointer = pointerWindow.GetChild(0).GetComponent<RectTransform>();

        // Set pointer icon as stuff sprite
        Image icon = pointer.GetChild(0).GetComponent<Image>();
        icon.sprite = iconSprite;
    }

    private void Update() 
    {
        targetPosition = transform.position;
        Vector3 toPosition = targetPosition;
        Vector3 formPosition = Camera.main.transform.position;
        formPosition.z = 0;
        
        Vector3 targetPositionScreenPoint = Camera.main.WorldToScreenPoint(targetPosition);
        bool isOffScreen = targetPositionScreenPoint.x <= 0 || targetPositionScreenPoint.x >= Screen.width || 
            targetPositionScreenPoint.y <= 0 || targetPositionScreenPoint.y >= Screen.height;

        if (isOffScreen)
        {
            // If target is off screen, change pointer position towards target and cap it to the screen
            Vector3 cappedTargetScreenPosition = targetPositionScreenPoint;
            if (cappedTargetScreenPosition.x <= BORDER_SIZE) cappedTargetScreenPosition.x = BORDER_SIZE;
            if (cappedTargetScreenPosition.x >= Screen.width - BORDER_SIZE) cappedTargetScreenPosition.x = Screen.width - BORDER_SIZE;
            if (cappedTargetScreenPosition.y <= BORDER_SIZE) cappedTargetScreenPosition.y = BORDER_SIZE;
            if (cappedTargetScreenPosition.y >= Screen.height - BORDER_SIZE) cappedTargetScreenPosition.y = Screen.height - BORDER_SIZE;

            pointer.position = cappedTargetScreenPosition;
            pointer.localPosition = new Vector3(pointer.localPosition.x, pointer.localPosition.y, 0);
        }
        else
        {
            // If target is on screen, destroy pointer window and disable script
            DestroyPointer();
        }
    }
}
