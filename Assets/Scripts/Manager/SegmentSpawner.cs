using UnityEngine;

// Class that spawns level segments from prefab on top of each other
public class SegmentSpawner : MonoBehaviour
{
    /* Prefabs should have an object called "EndPosition" to determine a point
       on which to spawn next segment */
    [SerializeField] private Transform startSegment;
    [SerializeField] private Transform segmentPrefab;

    private Vector3 lastSegmentEndPosition;

    public Vector3 GetLastSegmentEndPosition()
    {
        return lastSegmentEndPosition;
    }

    public Transform SpawnSegment()
    {
        // Spawn new level segment on it's last segment position
        Transform newSegment = Instantiate(segmentPrefab, lastSegmentEndPosition, Quaternion.identity);
        Vector3 moveSegment = Vector3.zero;
        moveSegment.y += Mathf.Abs(newSegment.Find("EndPosition").localPosition.y);
        newSegment.position += moveSegment;

        lastSegmentEndPosition = newSegment.Find("EndPosition").position;

        return newSegment;
    } 

    private void Awake() 
    {
        // Get the starting segment end point
        lastSegmentEndPosition = startSegment.Find("EndPosition").position;
    }
}
