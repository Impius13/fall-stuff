using UnityEngine;

// Class for special events where only chosen stuff falls
public class FallEvent : MonoBehaviour
{
    [SerializeField] private string eventName;
    [SerializeField] private string[] eventMessages;
    [SerializeField] private float eventLength;
    [SerializeField] private float lowerSpawnTimeRange;
    [SerializeField] private float upperSpawnTimeRange;

    private TextVisibility textName;
    private TextVisibility textMessage;
    private StuffPreset stuffPreset;

    private bool active = false;
    private float eventTime = 0;

    public void ActivateEvent()
    {
        // Activate the event
        active = true;
        eventTime = Time.time + eventLength;

        // Show event text
        textName.ShowMessage(eventName);
        textMessage.ShowMessage(eventMessages[Random.Range(0, eventMessages.Length)]);
    }

    public void SelfDestroy()
    {
        Destroy(gameObject);
        this.enabled = false;
    }

    public bool IsActive()
    {
        return active;
    }

    public float GetLowerSpawnTimeRange()
    {
        return lowerSpawnTimeRange;
    }

    public float GetUpperSpawnTimeRange()
    {
        return upperSpawnTimeRange;
    }

    public Transform GetItemToSpawn()
    {
        // Return random item from preset to spawn
        return stuffPreset.GetRandomObject();
    }

    private void Awake() 
    {
        textName = GameObject.Find("EventName").GetComponent<TextVisibility>();
        textMessage = GameObject.Find("EventMessage").GetComponent<TextVisibility>();

        stuffPreset = GetComponent<StuffPreset>();
    }

    private void Update() 
    {
        if (active && Time.time > eventTime)
        {
            active = false;
        }
    }
}
