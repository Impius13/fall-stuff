using UnityEngine;

// Class that spawn objects
public class ObjectSpawner : MonoBehaviour
{
    private float areaWidth = 0;
    private float areaHeight = 0;

    public void SetAreaWidth(float width)
    {
        areaWidth = width;
    }

    public void SetAreaHeight(float height)
    {
        areaHeight = height;
    }

    public Transform SpawnObject(Transform prefab)
    {
        // Spawn object from prefab above camera on random x position
        Vector3 position = Camera.main.transform.position;
        position.z = 0;
        position.x += Random.Range(-areaWidth / 2, areaWidth / 2);
        position.y += areaHeight * 2;

        return Instantiate(prefab, position, Quaternion.identity);
    }

    public Transform SpawnObject(Transform prefab, float yPosition)
    {
        // Spawn object from prefab on given y position and random x position 
        Vector3 position = Vector3.zero;
        position.x = Random.Range(-areaWidth / 2, areaHeight / 2);
        position.y = yPosition;

        return Instantiate(prefab, position, Quaternion.identity);
    }

    public Transform SpawnObjectBackgroundMenu(Transform prefab)
    {
        // Function for spawning objects on menu screen
        Vector3 position = Camera.main.transform.position;
        position.z = 0;
        position.x += Random.Range(-areaWidth / 2.6f, areaWidth / 2.6f);
        position.y += areaHeight;

        return Instantiate(prefab, position, Quaternion.identity);
    }

    public void RandomizeRotation(Transform obj)
    {
        // Randomly rotate stuff on z axis
        Vector3 rotation = Vector3.zero;
        rotation.z = Random.Range(0, 360);
        obj.transform.Rotate(rotation);
    }

    public void RandomizeDimensions(Transform obj)
    {
        // Randomly scale object
        float scale = Random.Range(-0.5f, 0.5f);
        obj.localScale += new Vector3(scale, scale);
    }
}
