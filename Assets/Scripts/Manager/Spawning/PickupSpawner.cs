using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    // Class that spawns pickup objects
    [SerializeField] private Transform lifePrefab;
    [SerializeField] private Transform[] powerUpPrefabs;
    [SerializeField] private float spawnRateTime;
    [SerializeField] private int lifeChanceToSpawn;
    [SerializeField] private int powerUpChanceToSpawn;

    private ObjectSpawner spawner;
    private float currentTimeToSpawn;

    private void Awake() 
    {
        spawner = GetComponent<ObjectSpawner>();
        currentTimeToSpawn = Time.time + spawnRateTime;
    }

    private void Update() 
    {
        // Every chosen timeframe randomly decide if something will spawn
        if (Time.time > currentTimeToSpawn)
        {
            if (Random.Range(0, 100) < lifeChanceToSpawn)
                spawner.SpawnObject(lifePrefab);

            else if (Random.Range(0, 100) < powerUpChanceToSpawn)
            {
                int itemIndex = Random.Range(0, powerUpPrefabs.Length);
                spawner.SpawnObject(powerUpPrefabs[itemIndex]);
            }

            currentTimeToSpawn = Time.time + spawnRateTime;
        }
    }
}
