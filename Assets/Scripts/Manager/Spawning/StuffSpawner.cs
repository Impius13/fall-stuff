using UnityEngine;

public class StuffSpawner : MonoBehaviour
{
    // Class that spawns stuff
    private const int HEIGHT_TO_INCREASE_DIFFICULTY = 5;
    private const float DECREASE_TIME_TO_SPAWN_BY = 0.2f;

    private ObjectSpawner spawner;
    private StuffPreset stuffPreset;

    private int totalWeight;
    private float timeToSpawn;
    private float currentTimeToSpawn;
    private FallEvent activeEvent;
    private bool eventActive = false;

    public float GetTimeToSpawn()
    {
        return timeToSpawn;
    }

    public void SetTimeToSpawn(float newTime)
    {
        timeToSpawn = newTime;
    }

    public void SetEvent(FallEvent newEvent)
    {
        activeEvent = newEvent;
        eventActive = true;
    }

    public void EndActiveEvent()
    {
        eventActive = false;
    }

    private void Awake() 
    {
        currentTimeToSpawn = Time.time + timeToSpawn;
    }

    private void Start() 
    {
        spawner = GetComponent<ObjectSpawner>();
        stuffPreset = GetComponent<StuffPreset>();

        // Get screen width and height from camera script
        CameraFollow camera = Camera.main.GetComponent<CameraFollow>();
        // Set spawner area
        spawner.SetAreaWidth(camera.GetScreenWidth());
        spawner.SetAreaHeight(camera.GetScreenHeight());
    }

    void Update()
    {
        if (Time.time > currentTimeToSpawn)
        {
            Transform spawnedObj;
            if (eventActive)
            {
                // If there is an active event, get item to spawn from it
                spawnedObj = spawner.SpawnObject(activeEvent.GetItemToSpawn());
                currentTimeToSpawn = Time.time + Random.Range(activeEvent.GetLowerSpawnTimeRange(), activeEvent.GetUpperSpawnTimeRange());
            }
            else
            {
                spawnedObj = spawner.SpawnObject(stuffPreset.GetRandomObject());
                currentTimeToSpawn = Time.time + timeToSpawn + Random.Range(-0.5f, 0.5f);
            }

            spawner.RandomizeRotation(spawnedObj);
        }
    }
}
