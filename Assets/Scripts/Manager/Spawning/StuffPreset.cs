using UnityEngine;

// Stores stuff preset with weight table
public class StuffPreset : MonoBehaviour
{
    [SerializeField] private Transform[] stuffPrefabs;
    [SerializeField] private int[] weightTable;

    private int totalWeight;

    public Transform GetRandomObject()
    {
        // Returns random object; chances are based on object weights
        int randomNum = Random.Range(0, totalWeight);
        Transform stuff = stuffPrefabs[0];
        for (int i = 0; i < weightTable.Length; i++)
        {
            if (randomNum <= weightTable[i])
            {
                stuff = stuffPrefabs[i];
                break;
            }
            else
                randomNum -= weightTable[i];
        }

        return stuff;
    }

    private void Awake() 
    {
        totalWeight = 0;
        foreach (int weight in weightTable)
        {
            totalWeight += weight;
        }
    }
}
