using UnityEngine;
using System.Collections.Generic;

// Generates wall segments and coins
public class LevelGenerator : MonoBehaviour
{
    private const float PLAYER_DISTANCE_TO_SPAWN_SEGMENT = 20f;
    private const float MINIMUM_DISTANCE_BETWEEN_CURRENCY = 1f;
    private const int MAXIMUM_CURRENCY_AMOUNT_ON_SEGMENT = 4;

    [SerializeField] private Transform currencyPrefab;
    [SerializeField] private PlayerController player;
    [SerializeField] private SegmentSpawner wallSegmentSpawner;
    [SerializeField] private SegmentSpawner backgroundSegmentSpawner;

    private ObjectSpawner spawner;

    /* private void SpawnCurrency(float lowerYRange, float upperYRange)
    {
        // Spawn currency on given Y axis range and random X axis
        float yPosition = Random.Range(lowerYRange, upperYRange);
        List<Transform> coins = new List<Transform>();

        coins.Add(spawner.SpawnObject(currencyPrefab, yPosition));
        bool spawn = true;
        int currencyAmount = 1;
        while (spawn = true && currencyAmount < MAXIMUM_CURRENCY_AMOUNT_ON_SEGMENT)
        {
            if (Random.Range(1, 100) < 50)
            {
                while(true)
                {
                    bool minimumDistance = true;
                    yPosition = Random.Range(lowerYRange, upperYRange);
                    foreach (Transform coin in coins) {
                        if (Mathf.Abs(coin.position.y - yPosition) < MINIMUM_DISTANCE_BETWEEN_CURRENCY) {
                            minimumDistance = false;
                            break;
                        }
                    }

                    if (minimumDistance)
                        break;
                }

                coins.Add(spawner.SpawnObject(currencyPrefab, yPosition));
                currencyAmount += 1;
            }
            else
            {
                spawn = false;
            }
        }
    } */

    private void Awake() 
    {
        spawner = GetComponent<ObjectSpawner>();
    }

    private void Update()
    {
        Vector3 lastWallEndPosition = wallSegmentSpawner.GetLastSegmentEndPosition();
        Vector3 lastBackgroundEndPosition = backgroundSegmentSpawner.GetLastSegmentEndPosition();

        if (Vector3.Distance(player.GetPosition(), lastWallEndPosition) < PLAYER_DISTANCE_TO_SPAWN_SEGMENT)
        {
            // Spawn next wall segment
            wallSegmentSpawner.SpawnSegment();
            Vector3 newSegmentEndPosition = wallSegmentSpawner.GetLastSegmentEndPosition();

            // Also spawn some currency
            //SpawnCurrency(lastWallEndPosition.y, newSegmentEndPosition.y);
        }

        if (Vector3.Distance(player.GetPosition(), lastBackgroundEndPosition) < PLAYER_DISTANCE_TO_SPAWN_SEGMENT)
        {
            // Spawn next background segment
            backgroundSegmentSpawner.SpawnSegment();
        }
    }
}
