using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Manager : MonoBehaviour
{
    // Manages scenes, UI elements and checks if it's game over
    [SerializeField] private PlayerStats stats;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private GameObject settingsScreen;
    [SerializeField] private GameObject tutorialScreen1;
    [SerializeField] private GameObject tutorialScreen2;
    [SerializeField] private SceneHandler sceneHandler;
    [SerializeField] private GameObject eventTextBox;
 
    private bool settingsMenuActive = false;
    private float timeScale;

    public void GameOver()
    {
        // Display game over screen
        gameOverScreen.SetActive(true);

        eventTextBox.SetActive(false);
        // Check if highscore is beaten; if yes, save it
        int highscore = PlayerPrefs.GetInt("highscore", 0);
        int currentScore = stats.GetScore();
        if (currentScore > highscore)
        {
            // Save highscore and display 'New highscore' text
            PlayerPrefs.SetInt("highscore", currentScore);

            Transform highscoreText = gameOverScreen.transform.Find("NewHighscore");
            highscoreText.gameObject.SetActive(true);
        }
    }

    public void ShowSettings()
    {
        // Display settings screen and pause the game
        if (!settingsMenuActive)
        {
            settingsMenuActive = true;
            settingsScreen.SetActive(true);
            timeScale = Time.timeScale;
            Time.timeScale = 0;
            // If event is active, hide the message
            eventTextBox.SetActive(false);
        }
    }

    public void ShowTutorial()
    {
        // Show tutorial if its the first time playing
        int firstTime = PlayerPrefs.GetInt("showTutorial", 0);
        if (firstTime == 0)
        {
            tutorialScreen1.SetActive(true);
            timeScale = Time.timeScale;
            Time.timeScale = 0;
            PlayerPrefs.SetInt("showTutorial", 1);
        }
    }

    public void SecondTutorialScreen()
    {
        // Show second tutorial screen
        tutorialScreen1.SetActive(false);
        tutorialScreen2.SetActive(true);
    }

    public void Resume()
    {
        // Hide screens and resume game
        settingsScreen.SetActive(false);
        tutorialScreen2.SetActive(false);

        Time.timeScale = timeScale;
        settingsMenuActive = false;
        eventTextBox.SetActive(true);
    }

    public void Restart()
    {
        DisableSettings();
        sceneHandler.StartGame();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadMenu()
    {
        DisableSettings();
        sceneHandler.LoadMenu();
        //SceneManager.LoadScene("Menu");
    }

    private void DisableSettings()
    {
        // Reset time scale
        if (settingsMenuActive)
        {
            Time.timeScale = 1f;
            settingsMenuActive = false;
        }
    }

    private void Start() 
    {
        // Add method to scene handler delegate
        sceneHandler.onFinishedSceneTransition += ShowTutorial;
    }

    private void Update() 
    {
        if (stats.IsDead())
        {
            GameOver();
        }
    }
}
