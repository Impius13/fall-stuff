using UnityEngine;

public class GameLoopManager : MonoBehaviour
{
    // Controls difficulty, game levels and events
    [SerializeField] private PlayerController player;
    [SerializeField] private Transform[] events; 
    [SerializeField] private float startingTimeToSpawn = 3;
    [SerializeField] private float timeToSpawnDecrement = 0.2f;
    [SerializeField] private float maxDifficultyTime;
    [SerializeField] private int heightStepToIncreaseDifficulty = 10;
    [SerializeField] private float eventLaunchTimeRate;
    [SerializeField] private int eventLaunchChance;

    private StuffSpawner spawner;
    private float highestPlayerHeight;
    private int nextHeightToIncreaseDifficulty;
    private float nextTimeToLaunchEvent;
    private float currentTimeToSpawn;
    private FallEvent activeEvent;

    private void LaunchEvent()
    {
        // Get random event from prefabs and launch it
        activeEvent = Instantiate(events[Random.Range(0, events.Length)], transform.position, Quaternion.identity).GetComponent<FallEvent>();
        activeEvent.ActivateEvent();

        // Set event on spawner
        spawner.SetEvent(activeEvent);

        Debug.Log("Event started");
    }

    private void IncreaseDifficulty()
    {
        // Decrease time to spawn next stuff
        currentTimeToSpawn -= timeToSpawnDecrement;
        if (currentTimeToSpawn < maxDifficultyTime)
            currentTimeToSpawn = maxDifficultyTime;

        if (activeEvent == null)
            spawner.SetTimeToSpawn(currentTimeToSpawn);

        nextHeightToIncreaseDifficulty += heightStepToIncreaseDifficulty;

        Debug.Log("DIFFICULTY INCREASED!");
    }

    private void Awake() 
    {
        spawner = GetComponent<StuffSpawner>();
        spawner.SetTimeToSpawn(startingTimeToSpawn);

        currentTimeToSpawn = startingTimeToSpawn;

        nextHeightToIncreaseDifficulty = heightStepToIncreaseDifficulty;
        nextTimeToLaunchEvent = Time.time + eventLaunchTimeRate;
    }

    private void Update() 
    {
        if (player.GetHeight() > highestPlayerHeight)
        {
            highestPlayerHeight = (int)player.GetHeight();
        }

        if (highestPlayerHeight >= nextHeightToIncreaseDifficulty && spawner.GetTimeToSpawn() != maxDifficultyTime)
        {
            // Increase difficulty if height level is hit and it's not already max difficulty
            IncreaseDifficulty();
        }

        if (activeEvent == null && Time.time > nextTimeToLaunchEvent)
        {
            if (Random.Range(0, 100) <= eventLaunchChance)
                LaunchEvent();
            else
                nextTimeToLaunchEvent = Time.time + eventLaunchTimeRate;
        }

        if (activeEvent != null && !activeEvent.IsActive())
        {
            // End event
            spawner.EndActiveEvent();

            activeEvent.SelfDestroy();
            activeEvent = null;

            nextTimeToLaunchEvent = Time.time + eventLaunchTimeRate;

            Debug.Log("Event ended");
        }
    }
}
