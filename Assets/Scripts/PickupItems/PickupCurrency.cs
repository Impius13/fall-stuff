using UnityEngine;

public class PickupCurrency : PickupItem
{
    // Adds currency to player
    [SerializeField] private int value = 1;

    protected override void Effect(GameObject player)
    {
        player.GetComponentInParent<PlayerStats>().AddCurrency(value);
    }
}
