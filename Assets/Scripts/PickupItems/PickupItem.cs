using UnityEngine;

public class PickupItem : MonoBehaviour
{
    // Base class for pickup items
    [SerializeField] protected PowerUp powerUp;

    private AudioSource sound;
    private bool collided = false;

    protected virtual void Effect(GameObject player)
    {
    }

    private void Destroy()
    {
        // Destroy object and disable script
        Destroy(gameObject);
        this.enabled = false;
    }

    private void DisablePhysical()
    {
        // Disable sprite, collider and particle system
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        if (LayerMask.LayerToName(gameObject.layer) != "Currency")
        {
            var emission = GetComponent<ParticleSystem>().emission;
            emission.enabled = false;
        }
    }

    private void Awake() 
    {
        sound = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if (LayerMask.LayerToName(other.gameObject.layer) == "Player" &&
            !collided)
        {
            // After collision with player, apply effect
            other.gameObject.GetComponentInParent<PowerUps>().ActivatePowerUp(powerUp);
            sound.Play();

            collided = true;
            // Disable physical properties and destroy object when sound clip is finished
            DisablePhysical();
            Invoke("Destroy", sound.clip.length);
        }
    }
}