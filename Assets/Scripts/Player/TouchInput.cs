using UnityEngine;

public class TouchInput : MonoBehaviour
{
    // Class for processing touch input
    [SerializeField] private float minSwipeDistance;

    private Vector2 singleStartSwipePosition;
    private Vector2 singleEndSwipePosition;
    private Vector2 doubleStartSwipePosition;
    private Vector2 doubleEndSwipePosition;
    private float swipeLength;
    private float horizontalAxis = 0;
    private bool swipedUp = false;

    public float GetHorizontalAxis()
    {
        return horizontalAxis;
    }

    public bool SwipedUp()
    {
        return swipedUp;
    }

    private Vector3 GetTouchPosition(Touch touch)
    {
        // Get position of touch point and convert it to world coordinates
        Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
        touchPosition.z = 0f;

        return touchPosition;
    }

    private void CheckSwipeUp(Vector2 startSwipePosition, Vector2 endSwipePosition)
    {
        // Check if player swiped up
        Vector2 distance = endSwipePosition - startSwipePosition;
        float xDistance = Mathf.Abs(distance.x);
        float yDistance = distance.y;

        if (yDistance > xDistance && yDistance > 0)
        {
            swipedUp = true;
        }
    }

    private void Update() 
    {
        // Process touch info
        horizontalAxis = 0;
        swipedUp = false;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Touch jump = touch;
            if (Input.touchCount > 1)
                jump = Input.GetTouch(1);

            Vector3 touchPosition = GetTouchPosition(touch);

            if (touch.phase == TouchPhase.Began)
            {
                singleStartSwipePosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                singleEndSwipePosition = touch.position;

                swipeLength = (singleEndSwipePosition - singleStartSwipePosition).magnitude;
                if (swipeLength > minSwipeDistance)
                {
                    CheckSwipeUp(singleStartSwipePosition, singleEndSwipePosition);
                }
            }

            if (Input.touchCount > 1)
            {
                if (jump.phase == TouchPhase.Began)
                {
                    doubleStartSwipePosition = jump.position;
                }
                else if (jump.phase == TouchPhase.Ended)
                {
                    doubleEndSwipePosition = jump.position;

                    swipeLength = (doubleEndSwipePosition - doubleStartSwipePosition).magnitude;
                    if (swipeLength > minSwipeDistance)
                    {
                        CheckSwipeUp(doubleStartSwipePosition, doubleEndSwipePosition);
                    }
                }
            }

            horizontalAxis = touchPosition.x - gameObject.transform.position.x;
            horizontalAxis = Mathf.Sign(horizontalAxis);
        }
    }
}
