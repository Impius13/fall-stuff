using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that controls player movement and animations
public class PlayerController : MonoBehaviour
{
    private const float JUMP_PAUSE = 0.5f;
    private const int INVULNERABILITY_TIME = 3;

    [SerializeField] private Rigidbody2D body;
    [SerializeField] private Transform legs;

    private Animator animator;
    private TouchInput input;
    private HeadCollisions headCollisions;
    private RenderController renderController;
    private PowerUps powerUps;
    private PlayerStats stats;
    private GroundCheck groundCheck;

    private bool isOnGround;
    private bool jump;
    private bool jumped;
    private float horizontalAxis;
    private bool invulnerability = false;
    private bool died = false;
    private bool stunned = false;

    public Vector3 GetPosition()
    {
        // Return current player position
        return body.transform.position;
    }

    public float GetHeight()
    {
        // Return current player y coordinate
        return body.transform.position.y;
    }

    private void Move(Vector2 direction)
    {
        // Move the player in given direction
        float speed = stats.GetMoveSpeed();

        if (powerUps.IsActiveSpeedUp())
            // If speed power up is active
            speed += powerUps.GetSpeedUpIncrease();

        if (powerUps.IsActiveSlowTime())
            // Increase speed times 2 if slow time is active
            speed += speed * 2;

        body.AddForce(direction * speed * Time.deltaTime);

        // Also add force to legs
        Rigidbody2D[] legRbs = legs.GetComponentsInChildren<Rigidbody2D>();
        foreach (Rigidbody2D rb in legRbs)
        {
            rb.AddForce(direction * speed * Time.deltaTime);
        }
    }

    private void Jump()
    {
        // Jumping
        float force = 0;

        // Jump if player is standing on ground
        if (isOnGround)
        {
            force = stats.GetJumpForce();
        }
        // If double jump is active
        else if (!isOnGround && powerUps.CanDoubleJump())
        {
            force = stats.GetJumpForce();
            powerUps.DoubleJumped();
        }

        if (force != 0)
        {
            // Add force to all player rigidbodies
            Rigidbody2D[] rbs = GetComponentsInChildren<Rigidbody2D>();
            foreach (Rigidbody2D rb in rbs)
            {
                rb.AddForce(new Vector2(0, force), ForceMode2D.Impulse);
            }
        }
    }

    private void DisableBalance()
    {
        // Disable all balance scripts
        Balance[] balanceScripts = GetComponentsInChildren<Balance>();
        foreach (Balance script in balanceScripts)
        {
            script.enabled = false;
        }
    }

    private void EnableBalance()
    {
        // Enable all balance scripts
        Balance[] balanceScripts = GetComponentsInChildren<Balance>();
        foreach (Balance script in balanceScripts)
        {
            script.enabled = true;
        }
    }

    private void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
        input = gameObject.GetComponent<TouchInput>();
        headCollisions = gameObject.GetComponentInChildren<HeadCollisions>();
        renderController = gameObject.GetComponent<RenderController>();
        powerUps = gameObject.GetComponent<PowerUps>();
        stats = gameObject.GetComponent<PlayerStats>();
        groundCheck = gameObject.GetComponent<GroundCheck>();
    }

    private void Update()
    {
        // Disable all input if player is stunned or dead
        if (!died)
        {
            if (!stunned)
            {
                isOnGround = groundCheck.IsOnGround();

                // Get input values 
                horizontalAxis = input.GetHorizontalAxis();
                jump = input.SwipedUp();

                // Move player
                if (horizontalAxis > 0)
                {
                    animator.Play("WalkRight");
                    Move(Vector2.right);
                }
                else if (horizontalAxis < 0)
                {
                    animator.Play("WalkLeft");
                    Move(Vector2.left);
                }
                else
                    animator.Play("Idle");

                // Jump
                if (jump && !jumped)
                {
                    Jump();
                    StartCoroutine(ResetJump());
                }

                // Check if stuff collided with player head
                if (headCollisions.CheckCollision() && !invulnerability)
                {
                    headCollisions.CollisionChecked();
                    DisableBalance();
                    StartCoroutine(Stun(2));
                    renderController.StunEffect();
                    stats.DecreaseLives();
                }

                if (invulnerability)
                {
                    // If invulnerable, mark any collision as checked
                    headCollisions.CollisionChecked();
                }
            }
            else
            {
                horizontalAxis = 0;
            }

            if (stats.IsDead())
            {
                died = true;
                StopAllCoroutines();
                DisableBalance();
                powerUps.DeactivateAllPowerUps();
                stunned = true;
            }
        }
    }

    private IEnumerator ResetJump()
    {
        jumped = true;
        yield return new WaitForSeconds(JUMP_PAUSE);
        jumped = false;
    }

    private IEnumerator Invulnerability()
    {
        invulnerability = true;
        yield return new WaitForSeconds(INVULNERABILITY_TIME);
        invulnerability = false;
        renderController.StopBlinking();
    }

    private IEnumerator Stun(float stunTime)
    {
        stunned = true;
        yield return new WaitForSeconds(stunTime);
        stunned = false;
        EnableBalance();

        // Grant player an invulnerability time
        renderController.StartBlinking();
        StartCoroutine(Invulnerability());
    }
}