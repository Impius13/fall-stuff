using UnityEngine;

// Class that check if object is standing on ground
public class GroundCheck : MonoBehaviour
{
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundMask;

    public bool IsOnGround()
    {
        // Makes a small capsule under object that cheks if it's touching another object with ground layer mask
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.2f, 0.1f), CapsuleDirection2D.Horizontal, 0, groundMask);
    }
}
