using UnityEngine;

public class Balance : MonoBehaviour
{
    // Class for balancing ragdoll body parts
    [SerializeField] private float targetRotation;
    [SerializeField] private float force;

    private Rigidbody2D rb;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rb.MoveRotation(Mathf.LerpAngle(rb.rotation, targetRotation, force * Time.fixedDeltaTime));
    }
}
