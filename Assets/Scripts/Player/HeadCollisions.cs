using UnityEngine;

public class HeadCollisions : MonoBehaviour
{
    // Checks for collisions with character head collider
    [SerializeField] private LayerMask stuffLayer;

    private bool collision = false;

    public bool CheckCollision()
    {
        return collision;
    }

    public void CollisionChecked()
    {
        collision = false;
    }

    public bool CheckIfStuck()
    {
        // Check if there is stuff directly above player head
        Collider2D[] colliders =  Physics2D.OverlapAreaAll(new Vector2(transform.position.x - 0.3f, transform.position.y),
            new Vector2(transform.position.x + 0.3f, transform.position.y + 10), stuffLayer);

        if (colliders.Length > 1)
            return true;

        return false;
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        // Check only collisions with stuff that did not already fell on ground
        if (other.gameObject.layer == LayerMask.NameToLayer("Stuff")
            && !other.gameObject.GetComponent<StuffCollision>().Fell())
        {
            collision = true;
            other.gameObject.GetComponent<StuffCollision>().PlayImpactSound();
        }
    }
}
