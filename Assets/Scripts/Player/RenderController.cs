using UnityEngine;

public class RenderController : MonoBehaviour
{
    // Class that controls attached sprite renderers
    private const float BLINK_RATE = 0.07f;

    [SerializeField] private ParticleSystem stunEffect;

    private SpriteRenderer[] renderers;
    private bool blink;

    public void StartBlinking()
    {
        // Start blinking renderers
        InvokeRepeating("BlinkRenderers", 0, BLINK_RATE);
    }

    public void StopBlinking()
    {
        // Stop blinking
        CancelInvoke();
        if (blink)
        {
            blink = false;
            ChangeRenderers();
        }
    }

    public void StunEffect()
    {
        // Play stun particles
        stunEffect.Play();
    }

    private void ChangeRenderers()
    {
        // Change status of renderers according to blink variable
        foreach (SpriteRenderer renderer in renderers)
        {
            renderer.enabled = !blink;
        }
    }

    private void BlinkRenderers()
    {
        // Negate blink variable and turn renderer accordingly
        blink = !blink;

        ChangeRenderers();
    }

    private void Awake() 
    {
        // Get attached renderers
        renderers = GetComponentsInChildren<SpriteRenderer>();
        blink = false;
    }
}
