using UnityEngine;
using UnityEngine.UI;

// Class that cheks if the player loses the game
public class StuckCheck : MonoBehaviour
{
    private const float STUCK_TIME_GAMEOVER = 5;

    [SerializeField] private LayerMask stuffMask;
    [SerializeField] private Rigidbody2D body;

    private HeadCollisions headCol;
    private PlayerStats playerStats;

    private float timeToLoose = 0;
    private float horizontalVelocity = 0;

    private void Start() 
    {
        headCol = GetComponentInChildren<HeadCollisions>();
        playerStats = GetComponent<PlayerStats>();
    }

    private void Update() 
    {
        // If player is barely moving and above him is stuff, begin game over countdown
        horizontalVelocity = Mathf.Abs(body.velocity.x);

        if (headCol.CheckIfStuck() && horizontalVelocity < 1 && timeToLoose == 0)
        {
            timeToLoose = Time.time + STUCK_TIME_GAMEOVER;
        }
        else if (!headCol.CheckIfStuck())
        {
            timeToLoose = 0;
        }

        if (timeToLoose != 0 && Time.time > timeToLoose)
        {
            playerStats.DecreaseLives(playerStats.GetLives());
        }

        if (timeToLoose > 0)
            Debug.Log("PLAYER STUCK, GAME OVER IN " + (timeToLoose - Time.time) + " seconds");
    }
}
