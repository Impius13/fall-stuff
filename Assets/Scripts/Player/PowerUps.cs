using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Class that controls players power ups
public class PowerUps : MonoBehaviour
{
    private const int DOUBLE_JUMP_INDEX = 0;
    private const int SPEED_INDEX = 1;
    private const int SLOW_TIME_INDEX = 2;

    private GroundCheck groundCheck;

    // Dictionary for aative powerups so the types can be indexed
    private Dictionary<int, PowerUp> activePowerupsIndexed = new Dictionary<int, PowerUp>();
    // List of active powerups for setting up UI indicators
    private List<PowerUp> activePowerups = new List<PowerUp>();

    // Double jump variables
    private bool activeDoubleJump = false;
    private bool doubleJumped = false;
    private float doubleJumpTime = 0;
    // Speed up variables
    private bool activeSpeedUp = false;
    private float speedUpIncrease = 0;
    private float speedUpTime = 0;
    // Slow time variables
    private float defaultTimeScale;
    private float defaultFixedDeltaTime;
    private bool activeSlowTime = false;
    private float slowTime = 0;

    public delegate void OnPowerUpChange(List<PowerUp> activePowerups);
    public OnPowerUpChange onPowerUpChangeCallback;

    // Double jump methods
    private void ActivateDoubleJump(float time)
    {
        activeDoubleJump = true;

        doubleJumpTime = Time.time + time;
    }

    private void DeactivateDoubleJump()
    {
        activeDoubleJump = false;
        RemovePowerup(DOUBLE_JUMP_INDEX);

        Debug.Log("DOUBLE JUMP DEACTIVATED");
    }

    public void DoubleJumped()
    {
        doubleJumped = true;
    }

    public bool CanDoubleJump()
    {
        return activeDoubleJump && !doubleJumped;
    }

    // Speed up methods
    private void ActivateSpeedUp(float time, float speedIncrease)
    {
        activeSpeedUp = true;

        speedUpIncrease = speedIncrease;
        speedUpTime = Time.time + time;
    }

    private void DeactivateSpeedUp()
    {
        activeSpeedUp = false;
        RemovePowerup(SPEED_INDEX);

        Debug.Log("SPEED UP DEACTIVATED");
    }

    public bool IsActiveSpeedUp()
    {
        return activeSpeedUp;
    }

    public float GetSpeedUpIncrease()
    {
        return speedUpIncrease;
    }

    // Slow time methods
    private void ActivateSlowTime(float time, float timeScaleMultiplier)
    {
        Time.timeScale = defaultTimeScale * timeScaleMultiplier;
        Time.fixedDeltaTime = defaultFixedDeltaTime * Time.timeScale;

        activeSlowTime = true;

        float timeScaled = time * Time.timeScale;
        slowTime = Time.time + timeScaled;
    }

    private void DeactivateSlowTime()
    {
        // Reset time scale
        Time.timeScale = defaultTimeScale;
        Time.fixedDeltaTime = defaultFixedDeltaTime;

        activeSlowTime = false;
        RemovePowerup(SLOW_TIME_INDEX);

        Debug.Log("TIME IS BACK TO NORMAL");
    }

    public bool IsActiveSlowTime()
    {
        return activeSlowTime;
    }

    public void ActivatePowerUp(PowerUp powerUp)
    {
        // Apply effect based on powerup type
        if (powerUp.type == PowerUp.PowerUpType.DoubleJump)
        {
            ActivateDoubleJump(powerUp.time);

            if (!activePowerupsIndexed.ContainsKey(DOUBLE_JUMP_INDEX))
                AddPowerup(DOUBLE_JUMP_INDEX, powerUp);
        }
        else if (powerUp.type == PowerUp.PowerUpType.Speed)
        {
            ActivateSpeedUp(powerUp.time, powerUp.value);

            if (!activePowerupsIndexed.ContainsKey(SPEED_INDEX))
                AddPowerup(SPEED_INDEX, powerUp);
        }
        else if (powerUp.type == PowerUp.PowerUpType.SlowTime)
        {
            ActivateSlowTime(powerUp.time, powerUp.value);

            if (!activePowerupsIndexed.ContainsKey(SLOW_TIME_INDEX))
                AddPowerup(SLOW_TIME_INDEX, powerUp);
        }
        else
        {
            // Life pickup
            GetComponent<PlayerStats>().IncreaseLives();
        }
    }

    public void DeactivateAllPowerUps()
    {
        if (activeDoubleJump)
            DeactivateDoubleJump();
        if (activeSpeedUp)
            DeactivateSpeedUp();
        if (activeSlowTime)
            DeactivateSlowTime();
    }

    // Add powerup to both data structures
    private void AddPowerup(int index, PowerUp powerUp)
    {
        activePowerups.Add(powerUp);
        activePowerupsIndexed.Add(index, powerUp);

        onPowerUpChangeCallback.Invoke(activePowerups);
    }

    // Removes powerup by its index from both data structures
    private void RemovePowerup(int index)
    {
        PowerUp powerUp = activePowerupsIndexed[index];
        activePowerups.Remove(powerUp);
        activePowerupsIndexed.Remove(index);

        onPowerUpChangeCallback.Invoke(activePowerups);
    }

    private void Awake()
    {
        groundCheck = GetComponent<GroundCheck>();

        defaultTimeScale = Time.timeScale;
        defaultFixedDeltaTime = Time.fixedDeltaTime;
    }

    private void Update() 
    {
        // Double jump checks
        if (activeDoubleJump && groundCheck.IsOnGround())
            doubleJumped = false;

        if (activeDoubleJump && Time.time > doubleJumpTime)
        {
            DeactivateDoubleJump();
        }

        // Speed up checks
        if (activeSpeedUp && Time.time > speedUpTime)
        {
            DeactivateSpeedUp();
        }

        // Slow time checks
        if (activeSlowTime && Time.time > slowTime)
        {
            DeactivateSlowTime();
        }
    }
}
