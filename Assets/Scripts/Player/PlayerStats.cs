using UnityEngine;

// Keeps track of all player stats and displays it
public class PlayerStats : MonoBehaviour
{
    [SerializeField] private int lives;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private TextUpdate livesText;
    [SerializeField] private TextUpdate scoreText;
    //[SerializeField] private TextUpdate currencyText;

    private PlayerController controller;

    private int score = 0;
    private int currency = 0;
    private bool dead;

    public int GetScore()
    {
        return score;
    }

    public int GetCurrency()
    {
        return currency;
    } 

    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public float GetJumpForce()
    {
        return jumpForce;
    }

    public int GetLives()
    {
        return lives;
    }

    public bool IsDead()
    {
        return dead;
    }

    public void IncreaseLives()
    {
        if (!dead)
        {
            lives += 1;
            livesText.UpdateText(lives.ToString());
        }
    }

    public void DecreaseLives(int livesCount = 1)
    {
        if (!dead)
        {
            lives -= livesCount;

            if (lives <= 0)
            {
                lives = 0;
                dead = true;
            }
            // Update lives text
            livesText.UpdateText(lives.ToString());
        }
    }

    public void AddCurrency(int value)
    {
        currency += value;
    }

    private void Awake() 
    {
        controller = GetComponent<PlayerController>();    
    }

    private void Start() 
    {
        // Set starting lives
        livesText.UpdateText(lives.ToString());
    }

    private void Update() 
    {
        // Update score and currency
        int currentScore = Mathf.RoundToInt(controller.GetHeight() * 2);
        if (currentScore > score)
        {
            score = currentScore;
            scoreText.UpdateText(score.ToString());
        }

        // if (currency > int.Parse(currencyText.GetText()))
        // {
        //     currencyText.UpdateText(currency.ToString());
        // }
    }
}
