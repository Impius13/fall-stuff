using UnityEngine;
using UnityEngine.UI;
using TMPro;

// Simple class that updates text component of game object
public class TextUpdate : MonoBehaviour
{
    private TextMeshProUGUI text;

    public string GetText()
    {
        return text.text;
    }

    public void UpdateText(string newText)
    {
        text.text = newText;
    }

    private void Awake() 
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "0";
    }
}
