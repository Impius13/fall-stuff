using UnityEngine;
using UnityEngine.UI;
using TMPro;

// Script that show and hides text after some time
public class TextVisibility : MonoBehaviour
{
    private const float TIME_TO_HIDE_TEXT = 4f;

    private TextMeshProUGUI textComponent;

    public void ShowMessage(string message)
    {
        textComponent.text = message;
        textComponent.enabled = true;

        Invoke("HideText", TIME_TO_HIDE_TEXT);
    }

    private void HideText()
    {
        textComponent.enabled = false;
    }

    private void Awake() 
    {
        textComponent = GetComponent<TextMeshProUGUI>();
    }
}
