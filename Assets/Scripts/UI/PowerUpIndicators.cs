using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Class for managing powerup UI indicator images
public class PowerUpIndicators : MonoBehaviour
{
    // !!!Number of indicator icons should be the same as number of powerups in game
    [SerializeField] private Image[] indicatorIconsArray;
    [SerializeField] private PowerUps powerUps;

    private void UpdateIcons(List<PowerUp> updatedPowerUpList)
    {
        // Clear all slots
        foreach (Image image in indicatorIconsArray)
        {
            image.sprite = null;
            image.enabled = false;
        }

        // Fill slots with powerup images
        int currentIndex = 0;
        foreach (PowerUp powerUp in updatedPowerUpList)
        {
            Image currentImage = indicatorIconsArray[currentIndex];
            currentImage.sprite = powerUp.icon;
            currentImage.enabled = true;

            currentIndex++;
        }
    }

    private void Start() 
    {
        powerUps.onPowerUpChangeCallback += UpdateIcons;
    }
}
